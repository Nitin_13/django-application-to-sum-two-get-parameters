from django.conf.urls import patterns, url

from addnums import views

url(r'^getnum/$', 'addnums.views.getnum',name='getnum'),
url(r'^success/(?P<id>\d+)/$', 'addnums.views.success',name='success'),
