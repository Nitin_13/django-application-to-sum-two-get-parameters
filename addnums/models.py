from django.db import models

# Create your models here.

class Nums(models.Model):
    num1 = models.IntegerField("First number")
    num2 = models.IntegerField("Second number")
    class Meta:
        unique_together= ('num1','num2')
    def __unicode__(self):
        return "%d plus %d equals %d" %(self.num1,self.num2,self.num1+self.num2)
    
