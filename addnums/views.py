from django.template import Context,loader,RequestContext

from django.shortcuts import render_to_response, get_object_or_404
from django.forms import ModelForm
from django.http import HttpResponse, Http404,HttpResponseRedirect,HttpResponseNotFound
from addnums.models import Nums

class Addnumform(ModelForm):
    class Meta:
        model = Nums
        
def getnum(request):
    if request.POST:
        form = Addnumform(request.POST)
        if form.is_valid():
            f = form.save()
            return HttpResponseRedirect("/success/%d/" % f.id)
    else:
        form = Addnumform()
    return render_to_response('addnums/templates/addnums/getnum.html',context_instance=RequestContext(request,
                            {"form":form,
                            }))
                            
def success(request,id):
    nums = Nums.objects.get(pk=id)
    return render_to_response('addnums/templates/addnums/success.html',context_instance=RequestContext(request,{"nums":nums,}))
