from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

	#url(r'^addnums/', include('addnums.urls')),
    #url(r'^admin/', include(admin.site.urls)),
    url(r'^getnum/$', 'addnums.views.getnum',name='getnum'),
url(r'^success/(?P<id>\d+)/$', 'addnums.views.success',name='success'),
)
